#!/usr/bin/env python

"""
  @package phosim
  @file ZMXtoPhosim.py
  @brief python script to convert a ZEMAX file to PHOSIM optics_*.txt(sort of)
 
  @brief Created by:
  @author En-Hsin Peng (Purdue)
  @Updated Glenn Sembroski 2023-02-14 (GHS)
 
  @warning This code is not fully validated
  and not ready for full release.  Please
  treat results with great caution.

Usage: python ZMXtoPhosim.py zemax.zmx input.lis 

In general the ZEMAX.ZMX files as used by the ZEMAX  programs will be in
unicode. This python script expects an input zemax.zmx file to be in ascii.
Thus you need to convert the ZEMAX.ZMX unicode file to a zemax.zmx ascii
file. In linux that looks like:
    $> cat ZEMAX.ZMX | iconv -f utf-16 > zemacs.zmx
    Note: we could probably add this to this script, but not yet.

    NOTE:(GHS)  My and En-Hsin's interpretation of how to translate the
    zemac.zmx file is basically an educated guess. As far as I can tell
    there is no non-proprietary manual that describes the various options
    in the zemacx.zmx file. I (and En-Hsin before me) are making our
    best guess.

 input.lis is a user provided file which contains
       Column 1: Zemax surface number 
       Column 2: Surface name (M1, L1, etc) 
       Column 3: Surface type (mirror, lens, filter, det, grating) 
       Column 4: Coating file
       Column 5: Medium file
       Column 6: Material file

       The input.lis  file is provided by the user. It is almost an
       optics_*.txt file. The user needs to look through the zemax.zmx file
       looking for the 'SURF' entries. The user then picks out those surfaces
       that are of interest to include in the optics_*.txt file and includes
       a line for each such surface in the input.lis file as described
       above. The values in the input.lis file will be used in the
       optics_*.txt file that this program generates unless they are found
       to be superseded in the zemax.zmx file.
       It may be possible to automate this in the future. i.e. Remove the
       necessity of the input.lis file. But for now we do it this way.

       Example (From LSST) e.g.,
       23   M1    mirror  m1_protAl_Ideal.txt  air  none
       26   M2    mirror  m2_protAl_Ideal.txt  air  none
       29   M3    mirror  m3_protAl_Ideal.txt  air  none
       30   none  none    none                 air  none
       31   L1    lens    lenses.txt           silica_dispersion.txt  none
       32   L1E   lens    lenses.txt           air  none
       33   L2    lens    lenses.txt           silica_dispersion.txt  none
       34   L2E   lens    lenses.txt           air  none
       35   F     filter  filter_x.txt         silica_dispersion.txt  none
       36   FE    filter  none                 air  none
       37   L3    lens    lenses.txt           silica_dispersion.txt  none
       38   L3E   lens    lenses.txt           air  none
       39   D     det     detectorar.txt       air  none

       Notes: 1. This script assumes that the option values in mm unless a
       'UNIT' option is specified in the zemax.zmx file. In that case the
       values that will be inserted from the zemax.zmx file into the
       optics_*.txt will be converted to mm as necessary (we only support
       converting from inches for now). PHOSIM requires units in mm.
       2. It will overwrite any existing optics_*.txt in the current folder. 

"""

import sys, os, commands

class Surface(object):            #Values for a line in the optics_*.txt file
    def __init__(self,curv,disz,z,rout,rin,coni,an,med):
        self.curv=curv   #Spherical curvature of the surface 0.0 defined as
                         #flat in PHOSIM,otherwise as normal!
        self.disz=disz   #Offset to the next surface
        self.z=z         #Absolute z of next surface (I think)
        self.rout=rout   #Outer radius of surface
        self.rin=rin     #Inner radius of surface (hole)
        self.coni=coni   #Conical value for the surface(1.0 is parabola)
        self.an=an       #polynomial constants starting at 2nd degree
        self.medium=med  #Medium. Type of material folloing the surface

#This function Finds the next surface (surf) as specified in the input.lis
#file.
#Collect all the values for this surface including its relative offset
#from the previous input.lis specified surface.
def findSurface(zmx,surf,surf0,flt,med,zprev=0.0):
    
    curv=0.0        #Curvature of surface in mm. Note PHOSIM wants curvature
                    #set to 0 if it is a flat surface(curvature=infinity)
    rout=0.0        #Outer radius
    rin=0.0         #Inner radius
    coni=0.0        #Conic constant (==1 for parabolic)
    an=[0.0 for i in range(16)] #polynomial constants starting at 2nd degree

    medium = med   #Default medium type
    ################################################
    #The following is mainly about filling in the absolute z values of the
    #surfaces.

    disz=[0.0 for i in range(100)]  #relative offsets between surfaces.
    pzup=[[-1.0,-1.0,-1.0] for i in range(100)]  #Not sure what this is(GHS)
    readCurv=False

    #read DISZ, update THIC, CRVT
    #Restart at the beginning of the file. This is not particularly
    #efficient but files are small so this will take little time.
    #Note: I think this fails for flt>0. i.e. more than one versions of an
    #instrument
    for line in open(zmx).readlines():  
        if 'SURF ' in line:                #Beginning of the next surface
            s=int(float(line.split()[1]))  #Pickup zemacs surface number
        elif 'DISZ' in line:
            #collect offset to the next surface (following this one), 
            if line.split()[1]!='INFINITY': #Skip first surface,defaults
                                            #to 0.0
                disz[s]=float(line.split()[1]) #Save offset to next surface,
                                               #indexing by surface number
        elif 'THIC' in line:       #No idea what THIC is(GHS)
            l=line.split()
            if float(l[2])-1==flt:
                disz[int(float(l[1]))]=float(l[3])
        elif 'CRVT' in line:   #This must be an inverse radius of  curvature
            l=line.split()     #This is part of the multi telescope version
                               #analysis. I have no idea what its doing(GHS).
            if float(l[2])-1==flt and float(l[1])==surf:
                curv=-(1/float(l[3]))
                readCurv=True

        elif 'PZUP' in line:   #Not sure what this is.(GHS)
            l=line.split()
            pzup[s][0]=float(l[1])
            pzup[s][1]=float(l[2])
            pzup[s][2]=float(l[3])

    #PZUP
    #Nor sure how this is used. Anyway, pzup was defined as originally
    #loaded with all "-1.0" So if no PZUP were found above the following
    #is skipped and disz is left untouched.
    for i in range(surf):#Not sure whats this is about. (GHS)
        if pzup[i][0]>0:
            disz[i]=disz[int(pzup[i][0])]*pzup[i][1]+pzup[i][2]


    #Now we are going to use the disz to determine the absolute z location
    #of this surface.
    z=0.0
    if surf>surf0:  #Surf0 is first surface listed in the input.lis file.
                    #Going to let its z location be 0.
        for i in range(surf): #Note i's  this range is from 0 to surf-1. Thus
                              # at the end of this loop, z is the absolute
                              #position of the surface(With + z going up.
            if i==surf0:      #Make z relative to surf[0]
                z0=z          #Grab z of surf0 to use as a reference
            z-=disz[i]   #For PHOSIM invert direction. The sky is at +z.
                 
        z-=z0            #Remove base
    #So we end up with z as the distance between surf0 and surf
    #######################################    

    #We will us this value of z to determine the offset between this surface
    #and the previous input.lis specified surface as needed for the PHOSIM
    #optics_*.txt file.

#Read spherical parameters and size of this surface (and any inner hole in
#them)
    found=False
    #Reread the zemax.zmx file from the beginning. Not the most efficient but
    #we really don't care.
    for line in open(zmx).readlines():
        if 'SURF '+str(surf) in line:   #look for surface 'surf' in the
                                        #zemacs.zmx file.
            found=True
            continue
        if found:                       #We are in the surf block we want.
            if 'CURV' in line and readCurv==False: 
                if float(line.split()[1])!=0:      #Pick up spherical radius of
                    curv=-1/float(line.split()[1]) #curvature. Zemax CURV
                                                   #uses the negative inverse
                                                   #of the PHOSIM convention.
  
            #In the following we determine the inner and outer radius of the
            #circular surface. My guess is that the CLAP or FLAP option values
            #are the the actual 'active' area of the surface (Inner and Outer
            #radii) and the values given with the DIAM 
            #options give the outer obstructed radius limit of the surface.
            #Thus, for the PHOSIM optics_*.txt file we want to use the CLAP
            #or FLAP values unless we only have the DIAM option values. In
            #that case we will use those values as the best approximation for
            #the 'active' area of the surface.
            #Note: It seems from my perusal of zemax files that the CLAP
            #option will always come after the DIAM options. Thus the CLAP 
            #option values have priority
            elif 'CLAP' in line or 'FLAP' in line: 
                rout=float(line.split()[2])   #Outer radius of active area
                                              #of the surface
                rin=float(line.split()[1])    #Inner radius (of the hole) in
                                              #the surface
            elif 'DIAM' in line:
                rout=float(line.split()[1])   #Actual outer radius of surface
                                              #Used if no CLAP of FLAP option
                rin=0.0

            #Conical value of the surface (EX: for parabola == 1.0)
            elif 'CONI' in line:
                coni=float(line.split()[1])
            
            #polynomial constants starting at 2nd degree of surface
            elif 'PARM' in line:  #parm 1: a2, parm 2: a4
                lstr=line.split()
                an[int(float(lstr[1]))*2-1]=float(lstr[2])/1e3
            elif 'GLAS' in line:  #Get the glass type
                lstr=line.split()
                medium=lstr[1]+'.txt'
            elif 'SURF' in line or 'CONF' in line:
                    break   #Quit when we finish reading this  surfaces
                            #options.

    #Load up the surface structure
    surface=Surface(curv,(z-zprev),z,rout,rin,coni,an,medium)
    return surface


def printOptics(output,surface,name,typ,coating,medium,material,flt):
    out=open(output,'a')
    if typ=='det' and surface.rout==0:
        surface.rout=400.0
    out.write('%-7s %7s %8.1f %10.4f %10.4f %6.1f %9.6f '  % (name, typ,
               surface.curv*unitsConv, surface.disz*unitsConv,
               surface.rout*unitsConv, surface.rin*unitsConv, surface.coni))
    for i in range(14):
        out.write('%13.6e ' % (-surface.an[i+2]))
    if coating!='none' and typ=='filter':
        coating='filter_'+str(flt)+'.txt'
    out.write('%s %s %s\n' % (coating,medium,material))
    out.close()


#Start of Main code#
zmxFile=sys.argv[1]
inputList=sys.argv[2]

"""It is possible that the zemax.zmx file will have specifications for a
   number of versions of the instrument. This may correspond to the different
   optics_*.txt files (different filters for example).
"""
#Count the number of instrument versions. (All versions end with the MNUM
#   option in the zemac.zmx file.

fltNum=0.0
print('count number of MNUM instances in zemax file\n')
with open(zmxFile,"r") as fp:
    lines=fp.read()    # The .read() reads the whole file into a single string
    fltNum=lines.count("MNUM")    #Counts number of telescope versions
print('Num MNUM found: ' + str(fltNum))
units="mm"
unitConv=1.0
#Find if we have a :"UNIT" option specified in the zemax.zmx file
for line in open(zmxFile).readlines():
    if 'UNIT' in line:
        print('UNIT line: ' + line)
        units=line.split()[1]
        print('units: ' + units)
        if "IN" in units:
            print('Set conversion to mm from IN')
            unitsConv = 25.4
            break
        elif units == "MM":
            unitsConv=1.0   #mm
            break
        else :
            print('ZMXtoPHOSIM.py: Unknown units type specified in input .zmx file.\n')
            exit(1)
            
    else:
        #Assume units in zemax.zmx file in mm.
        unitsConv=1.0

print('unitsConv: '+str(unitsConv)+'\n')

#Setup arrays for the input.lis file

zmxSurface=[]  #ZEMAX file number id for each surface
name=[]        #surface names
typ=[]         #Surface types
coating=[]     #Coating file names
medium=[]      #Medium file names
material=[]    #material file names

#Read in the input.lis values.
for line in open(inputList).readlines():
    l=line.split()
    zmxSurface.append(int(float(l[0])))
    name.append(l[1])
    typ.append(l[2])
    coating.append(l[3])
    medium.append(l[4])
    material.append(l[5])


#Iterate over the various telescope versions. (optics_*.txt files)
#where * goes from 0 to fltNum-1 
for flt in range(fltNum):
    output='optics_'+str(flt)+'.txt'
    try:
        os.remove(output)
    except OSError:
        pass

    zprev=0.0       #Init the previous absolute  z  value.

    #Iterate over the surfaces listed in the input.lis file
    for i in range(len(name)):
        #Find the next surface that is listed in the input.lis file. We
        #have to keep track of the cumulative offset in z (position) from
        #the last listed surface. PHOSIM wants that value in the
        #optics_*.txt file
        
        #Fill in what we can for this surface from the zmemax.zmx file.
        surface=findSurface(zmxFile, zmxSurface[i], zmxSurface[0], flt, medium[i], zprev)
                            #Note: surface.medium now holds medium type.

        #Save previous absolute z of this surface
        zprev=surface.z

        #Write this surface out to the optics_*.txt file for this version
        #of the telescope
        printOptics(output,surface,name[i],typ[i],coating[i],surface.medium,
                    material[i],flt)

#And we are done.
