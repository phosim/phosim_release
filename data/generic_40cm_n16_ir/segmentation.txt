#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
chip 1     1024     1024
chip_amplifier 0     1023 0     1023 1 1 1 0 0 0 0 0 0 0 0 0 0 0 0 0 18 1 0 0 0 0 0 0
