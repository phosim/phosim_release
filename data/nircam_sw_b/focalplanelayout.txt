# focal plane geometry layout data
# 
# rows are the chips in the focal plane
# columns are the name, x position (microns), y position (microns),
# pixel size (microns), number of x pixels, number of y pixels,
# device material (silicon or MCT) 
# type of device (note CMOS also means Fast Frame CCD, and CCD with 0 readout time also means CMOS where you can set the exposure),
# readout integration mode (frame or sequence)
# readout time (CCD) or frame rate (CMOS)
# group of sensors, 3 euler rotations (degrees), 3 translations (mm)
# deformation type, deformation coefficients, QE variation (obsolete)
#
B1 18724 18724 18.0 2048 2048 MCT CMOS sequence 10.73677 8.0 Group0 0 0 0 0 0 0 sensor.txt 0.0
B2 18724 -18724 18.0 2048 2048 MCT CMOS sequence 10.73677 8.0 Group0 0 0 0 0 0 0 sensor.txt 0.0
B3 -18724 18724 18.0 2048 2048 MCT CMOS sequence 10.73677 8.0 Group0 0 0 0 0 0 0 sensor.txt 0.0
B4 -18724 -18724 18.0 2048 2048 MCT CMOS sequence 10.73677 8.0 Group0 0 0 0 0 0 0 sensor.txt 0.0
