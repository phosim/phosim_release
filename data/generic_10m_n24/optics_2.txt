#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics v    0.2    1.2 0.5 4103841.0000 1 1 0
primary mirror  480000.0000      0.0000   5000.0000    625.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens    -13800.0010 211560.0000    625.0000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 jc_v sio2 none
filter  lens    -13524.0010    600.0000    625.0000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000  27840.0000   3600.0000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
