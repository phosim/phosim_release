#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics k    0.8    6.0 0.5 3283072.7500 1 1 0
primary mirror  384000.0000      0.0000   4000.0000    500.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens    -11040.0010 168960.0000    500.0000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 mko_k sio2 none
filter  lens    -10819.2012    480.0000    500.0000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000  22560.0000   2880.0000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
