#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics j    0.8    6.0 0.5   41038.4102 1 1 0
primary mirror    4800.0000      0.0000     50.0000      6.2500 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens      -138.0000   2112.0000      6.2500      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 mko_j sio2 none
filter  lens      -135.2400      6.0000      6.2500      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000    282.0000     36.0000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
