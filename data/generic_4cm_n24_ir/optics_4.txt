#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics kp    0.8    6.0 0.5   16415.3633 1 1 0
primary mirror    1920.0000      0.0000     20.0000      2.5000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens       -55.2000    844.8000      2.5000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 mko_kp sio2 none
filter  lens       -54.0960      2.4000      2.5000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000    112.8000     14.4000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
