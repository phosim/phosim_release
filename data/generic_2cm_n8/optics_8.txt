#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics i    0.2    1.2 0.5    2735.8938 1 1 0
primary mirror     320.0000      0.0000     10.0000      1.2500 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens        -9.2000    140.8800      1.2500      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 sdss_i sio2 none
filter  lens        -9.0160      0.4000      1.2500      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000     18.7200      2.4000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
