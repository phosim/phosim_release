#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics k    0.8    6.0 0.5    5471.7876 1 1 0
primary mirror     640.0000      0.0000     10.0000      1.2500 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens       -18.4000    281.6000      1.2500      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 mko_k sio2 none
filter  lens       -18.0320      0.8000      1.2500      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000     37.6000      4.8000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
