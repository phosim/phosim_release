#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics r    0.2    1.2 0.5    1367.9469 1 1 0
primary mirror     160.0000      0.0000      5.0000      0.6250 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens        -4.6000     70.5600      0.6250      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 sdss_r sio2 none
filter  lens        -4.5080      0.2000      0.6250      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000      9.2400      1.2000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
