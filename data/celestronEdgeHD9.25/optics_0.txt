
#lable  surface curvature       z step          aperture                
#        type    radius          
optics none 0.30  1.20  0.5   42133.94  1 1 0
L1      lens    0.0             0.0             116.9          36.738074   0.0  0.0 0.0 0.0     0.0     0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  lenses.txt N_BK7.txt none
L1e     lens    134000.0        -3.5            116.919307     0.0         0.0  0.0 0.0 2.7e-13 8.0e-20 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  lenses.txt air       none
m1      mirror  1074.5          -400.0          120.089377     27.381227   0.0  0.0 0.0 0.0     0.0     0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  none       air       none
m2      mirror  395.0           386.6           36.738074      0.0         0.0  0.0 0.0 0.0     0.0     0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  none       air       none
L2      lens    -86.0           -440.0          27.381227      0.0         0.0  0.0 0.0 0.0     0.0     0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  lenses.txt N_SK2.txt none
L2e     lens    222.0           -10.0           27.381227      0.0         0.0  0.0 0.0 0.0     0.0     0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  lenses.txt air       none
L3      lens    333.0           -1.0            27.381227      0.0         0.0  0.0 0.0 0.0     0.0     0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  lenses.txt K10.txt   none
L3e     lens    -49.0           -6.0            27.381227      0.0         0.0  0.0 0.0 0.0     0.0     0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  lenses.txt air       none
D       det     0.0             -162.935828     20.218360      0.0         0.0  0.0 0.0 0.0     0.0     0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  none       air       none
