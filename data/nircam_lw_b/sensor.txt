# chip material properties (being tested)
compositionHgCd 0.2995
edgeSurfaceCharge -5e9
treeRingPeriod 3750.0
treeRingAmplitude 0.03
treeRingRatio 7.5
treeRingVariation 750.0
treeRingDecay 150000.0
deadLayerDepth 1.5
deadLayerWidth 3000.0
deadLayerHeight 500.0
deadLayerXoverlap -200.0
deadLayerYoverlap -200.0
deadLayerXinit 0 0.0
deadLayerXinit 1 300.0
deadLayerYinit 0 0.0
deadLayerYinit 1 300.0
deadLayerAngle 0 10.0
deadLayerAngle 1 10.0
pixelVarX 0.003
pixelVarY 0.002
wellDepth 83300
nbulk 1e12
nf -5e15
nb 0.0
sf 0.01
sb 0.0025
channelDepth 1e-5
stopMomentPerPixel 30
sensorTempNominal 37
xTransfer 1
yTransfer 1
