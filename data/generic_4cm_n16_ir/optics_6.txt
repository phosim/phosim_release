#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics lp    0.8    6.0 0.5   10943.5752 1 1 0
primary mirror    1280.0000      0.0000     20.0000      2.5000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens       -36.8000    563.2000      2.5000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 mko_lp sio2 none
filter  lens       -36.0640      1.6000      2.5000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000     75.2000      9.6000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
