#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics g    0.2    1.2 0.5     341.9867 1 1 0
primary mirror      40.0000      0.0000     10.0000      1.2500 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens        -1.1500     17.6600      1.2500      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 sdss_g sio2 none
filter  lens        -1.1270      0.0500      1.2500      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000      2.2900      0.3000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
