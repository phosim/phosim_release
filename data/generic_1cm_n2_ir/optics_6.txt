#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics lp    0.8    6.0 0.5     341.9867 1 1 0
primary mirror      40.0000      0.0000      5.0000      0.6250 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens        -1.1500     17.6000      0.6250      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 mko_lp sio2 none
filter  lens        -1.1270      0.0500      0.6250      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000      2.3500      0.3000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
