#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics y    0.8    6.0 0.5   34198.6758 1 1 0
primary mirror    4000.0000      0.0000    500.0000     62.5000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens      -115.0000   1760.0000     62.5000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 mko_y sio2 none
filter  lens      -112.7000      5.0000     62.5000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000    235.0000     30.0000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
