#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics z    0.2    1.2 0.5     170.9934 1 1 0
primary mirror      20.0000      0.0000      5.0000      0.6250 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens        -0.5750      8.8000      0.6250      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 sdss_z sio2 none
filter  lens        -0.5635      0.0250      0.6250      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000      1.1750      0.1500      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
