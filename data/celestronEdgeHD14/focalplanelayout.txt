#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
chip	0.0 0.0	3.76 9576 6388 silicon CCD frame 3.0 10.0	Group0 0.0 0.0 0.0 0.0 0.0 0.0 sensor.txt 0.0
