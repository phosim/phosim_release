#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics b    0.2    1.2 0.5    3419.8674 1 1 0
primary mirror     400.0000      0.0000    100.0000     12.5000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens       -11.5000    176.3000     12.5000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 jc_b sio2 none
filter  lens       -11.2700      0.5000     12.5000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000     23.2000      3.0000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
