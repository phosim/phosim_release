#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics mp    0.8    6.0 0.5   17099.3379 1 1 0
primary mirror    2000.0000      0.0000    500.0000     62.5000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens       -57.5000    880.0000     62.5000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 mko_mp sio2 none
filter  lens       -56.3500      2.5000     62.5000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000    117.5000     15.0000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
