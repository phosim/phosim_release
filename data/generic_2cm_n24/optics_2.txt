#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics v    0.2    1.2 0.5    8207.6816 1 1 0
primary mirror     960.0000      0.0000     10.0000      1.2500 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens       -27.6000    423.1200      1.2500      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 jc_v sio2 none
filter  lens       -27.0480      1.2000      1.2500      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000     55.6800      7.2000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
