#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics v    0.2    1.2 0.5   32830.7266 1 1 0
primary mirror    3840.0000      0.0000     40.0000      5.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens      -110.4000   1692.4800      5.0000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 jc_v sio2 none
filter  lens      -108.1920      4.8000      5.0000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000    222.7200     28.8000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
