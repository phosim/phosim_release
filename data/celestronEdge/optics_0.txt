
#lable  surface curvature       z step          aperture                
#        type    radius          
optics u	0.30  1.20  0.36  180000.0 1 1 0
L1      lens    0.0             0.0             140.0           0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  lenses.txt silica_dispersion.txt none
L1e     lens    200000.0        -6.652367       140.030580      0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  lenses.txt air none
m1      mirror  1140.0          -416.0          142.783771      0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  m1_protAl_Ideal.txt air none
m2      mirror  377.0           422.0           39.866581       0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  m1_protAl_Ideal.txt air none
L2      lens    -111.0          -399.0          29.270505       0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  lenses.txt silica_dispersion.txt none
L2e     lens    157.0           -8.0            29.270505       0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  lenses.txt air none
L3      lens    155.0           -2.0            29.270505       0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  lenses.txt silica_dispersion.txt none
L3e     lens    -70.0           -5.0            29.270505       0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  lenses.txt air none
D       det     0.0             -261.972311     19.502662       0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0 0.0  detectorar.txt air none

