#
# @package phosim
# @brief phosim data file
#
# @brief Created by:
# @author John R. Peterson (Purdue)
#
# @brief Modified by:
#
#  @warning Please treat results using PhoSim with caution as this project is in active development.
#  @warning This material is copyrighted and subject to a open source license with restrictions.  See COPYING for details.
#
optics g    0.2    1.2 0.5   10943.5752 1 1 0
primary mirror    1280.0000      0.0000     40.0000      5.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 protected_al air none
filter  lens       -36.8000    565.1200      5.0000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 sdss_g sio2 none
filter  lens       -36.0640      1.6000      5.0000      0.0000 -1 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 lens_ar air none
image   det          0.0000     73.2800      9.6000      0.0000  0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 0 detector_ar air none
