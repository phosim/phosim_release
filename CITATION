@ARTICLE{2015ApJS..218...14P,
       author = {{Peterson}, J.~R. and {Jernigan}, J.~G. and {Kahn}, S.~M. and {Rasmussen}, A.~P. and {Peng}, E. and {Ahmad}, Z. and {Bankert}, J. and {Chang}, C. and {Claver}, C. and {Gilmore}, D.~K. and {Grace}, E. and {Hannel}, M. and {Hodge}, M. and {Lorenz}, S. and {Lupu}, A. and {Meert}, A. and {Nagarajan}, S. and {Todd}, N. and {Winans}, A. and {Young}, M.},
        title = "{Simulation of Astronomical Images from Optical Survey Telescopes Using a Comprehensive Photon Monte Carlo Approach}",
      journal = {\apjs},
     keywords = {atmospheric effects, cosmology: observations, Galaxy: structure, instrumentation: detectors, surveys, telescopes, Astrophysics - Instrumentation and Methods for Astrophysics},
         year = 2015,
        month = may,
       volume = {218},
       number = {1},
          eid = {14},
        pages = {14},
          doi = {10.1088/0067-0049/218/1/14},
archivePrefix = {arXiv},
       eprint = {1504.06570},
 primaryClass = {astro-ph.IM},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2015ApJS..218...14P},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
@ARTICLE{2019ApJ...873...98P,
       author = {{Peterson}, J.~R. and {Peng}, E. and {Burke}, C.~J. and {Sembroski}, G. and {Cheng}, J.},
        title = "{Deformation of Optics for Photon Monte Carlo Simulations}",
      journal = {\apj},
     keywords = {atmospheric effects, telescopes, Astrophysics - Instrumentation and Methods for Astrophysics},
         year = 2019,
        month = mar,
       volume = {873},
       number = {1},
          eid = {98},
        pages = {98},
          doi = {10.3847/1538-4357/ab0418},
archivePrefix = {arXiv},
       eprint = {1902.09618},
 primaryClass = {astro-ph.IM},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2019ApJ...873...98P},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
@ARTICLE{2019JATIS...5c8002B,
       author = {{Burke}, Colin J. and {Peterson}, John R. and {Egami}, Eiichi and {Leisenring}, Jarron M. and {Sembroski}, Glenn H. and {Rieke}, Marcia J.},
        title = "{PhoSim-NIRCam: photon-by-photon image simulations of the James Webb Space Telescope's near-infrared camera}",
      journal = {Journal of Astronomical Telescopes, Instruments, and Systems},
     keywords = {Astrophysics - Instrumentation and Methods for Astrophysics},
         year = 2019,
        month = jul,
       volume = {5},
          eid = {038002},
        pages = {038002},
          doi = {10.1117/1.JATIS.5.3.038002},
archivePrefix = {arXiv},
       eprint = {1905.06461},
 primaryClass = {astro-ph.IM},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2019JATIS...5c8002B},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
@ARTICLE{2020ApJ...889..182P,
       author = {{Peterson}, J.~R. and {O'Connor}, P. and {Nomerotski}, A. and {Magnier}, E. and {Jernigan}, J.~G. and {Cheng}, J. and {Cui}, W. and {Peng}, E. and {Rasmussen}, A. and {Sembroski}, G.},
        title = "{Sensor Distortion Effects in Photon Monte Carlo Simulations}",
      journal = {\apj},
     keywords = {Astronomical simulations, Computational astronomy, Astronomical detectors, Astronomical instrumentation, 1857, 293, 84, 799, Astrophysics - Instrumentation and Methods for Astrophysics},
         year = 2020,
        month = feb,
       volume = {889},
       number = {2},
          eid = {182},
        pages = {182},
          doi = {10.3847/1538-4357/ab64e0},
archivePrefix = {arXiv},
       eprint = {2001.03134},
 primaryClass = {astro-ph.IM},
       adsurl = {https://ui.adsabs.harvard.edu/abs/2020ApJ...889..182P},
      adsnote = {Provided by the SAO/NASA Astrophysics Data System}
}
