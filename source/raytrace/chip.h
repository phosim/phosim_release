///
/// @package phosim
/// @file chip.h
/// @brief header file for chip class
///
/// @brief Created by:
/// @author John R. Peterson (Purdue)
///
/// @warning This code is not fully validated
/// and not ready for full release.  Please
/// treat results with caution.
///

class Chip {

public:
    int nampx;
    int nampy;
    int buffer;
    int midpoint;
    int extraX;
    int extraY;
    int drift;

};
