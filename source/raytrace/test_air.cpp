///
/// @package phosim
/// @file test_air.cpp
/// @brief Unit tests for air class.
///
/// @brief Created by:
/// @author John R. Peterson (Purdue)
///
/// @brief Modified by:
///
/// @warning Please treat results using PhoSim
/// with caution as this project is in active development.
///
/// @warning This material is copyrighted and
/// subject to a open source license with
/// restrictions.  See COPYING for details.
///

#include "validation/unittest.h"

int main() {
    return 0;
}
