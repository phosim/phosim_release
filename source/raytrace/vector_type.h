
#ifndef VECTYPE

struct Vector {
    double x;
    double y;
    double z;
};

struct TwoVector {
    double x;
    double y;
};

struct TwoVectorInt {
    int x;
    int y;
};


#define VECTYPE 1

#endif
